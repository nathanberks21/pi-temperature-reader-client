import './styles.scss';

import { select } from 'd3-selection';
import { scaleLinear } from 'd3-scale';
import { axisLeft, axisRight } from 'd3-axis';
import { curveMonotoneX, line } from 'd3-shape';

import { getDaylightProportions } from './daylight-calculator';
import { getHistoricalValues, getWeekData } from './temp-api';

const WIDTH = 1500;
const HEIGHT = 400;
const MARGINS = {
  top: 50,
  right: 50,
  bottom: 50,
  left: 50
};

const daylightValues = getDaylightProportions();

const endDate = new Date();
const startDate = new Date();
startDate.setDate(startDate.getDate() - 6);
startDate.setHours(0, 0, 0, 0);
endDate.setHours(23, 59, 59, 999);

const xScale = scaleLinear()
  .domain([startDate, endDate])
  .range([0, WIDTH]);

const yScale = scaleLinear()
  .domain([0, 40])
  .range([HEIGHT, 0]);

const svg = select('#graph')
  .append('svg')
  .attr('width', WIDTH + MARGINS.left + MARGINS.right)
  .attr('height', HEIGHT + MARGINS.top + MARGINS.bottom);

const axisContainer = svg
  .append('g')
  .attr('transform', `translate(${MARGINS.left}, ${MARGINS.top})`);

const rectContainer = svg
  .append('g')
  .attr('transform', `translate(${MARGINS.left}, ${MARGINS.top})`);

const pathContainer = svg
  .append('g')
  .attr('transform', `translate(${MARGINS.left}, ${MARGINS.top})`);

axisContainer
  .append('g')
  .classed('y-axis', true)
  .call(axisLeft(yScale));

axisContainer
  .append('g')
  .classed('y-axis', true)
  .attr('transform', `translate(${WIDTH},  0)`)
  .call(axisRight(yScale));

const daylightRects = rectContainer
  .selectAll('daylight-rect')
  .data(daylightValues);

daylightRects.exit().remove();

daylightRects
  .enter()
  .append('rect')
  .classed('daylight-rect', true)
  .attr('x', d => Math.ceil(d.start * WIDTH))
  .attr('y', 0)
  .attr('width', d => Math.ceil((d.end - d.start) * WIDTH))
  .attr('height', HEIGHT)
  .attr('fill', d => d.colour);

drawHistoricalLine();
drawHardwareTempLine();

const drawLine = line()
  .x(d => xScale(d.time))
  .y(d => yScale(d.celcius))
  .curve(curveMonotoneX);

async function drawHistoricalLine() {
  const historicalPoints = await getHistoricalValues();
  const graphValues = historicalPoints.reduce((acc, curr) => {
    const { data } = curr;
    return [...acc, ...data];
  }, []);

  pathContainer
    .datum(graphValues)
    .append('path')
    .classed('historical-path', true)
    .attr('d', drawLine);
}

async function drawHardwareTempLine() {
  const weekPoints = await getWeekData();
  const graphValues = weekPoints.map(({ date: time, celcius }) => ({
    time,
    celcius
  }));

  pathContainer
    .datum(graphValues)
    .append('path')
    .classed('hardware-temp-path', true)
    .attr('d', drawLine);
}
