import SunCalc from 'suncalc';

const OXFORD_LAT = 51.75;
const OXFORD_LNG = 1.26;
const MILLISECONDS_IN_DAY = 86400000;

export function getDaylightProportions() {
  let days = [];
  for (let i = 6; i >= 0; i--) {
    const date = new Date();
    const pastDate = date.getDate() - i;
    date.setDate(pastDate);

    days = [...days, ...getDayProportions(date, 6 - i)];
  }
  return days;
}

function getDayProportions(date, day) {
  const sunData = SunCalc.getTimes(date, OXFORD_LAT, OXFORD_LNG);
  const dateStart = new Date(date);
  dateStart.setHours(0, 0, 0, 0);
  const startMs = dateStart.getTime();
  const daylightData = [];

  if (sunData.nightEnd.getTime()) {
    daylightData.push(
      {
        title: 'night',
        start: calcNormalisedPeriod(0, day),
        end: calcNormalisedPeriod(sunData.nightEnd.getTime() - startMs, day),
        colour: '#2f454d'
      },
      {
        title: 'astroTwillight',
        start: calcNormalisedPeriod(sunData.nightEnd.getTime() - startMs, day),
        end: calcNormalisedPeriod(
          sunData.nauticalDawn.getTime() - startMs,
          day
        ),
        colour: '#586a70'
      }
    );
  } else {
    daylightData.push({
      title: 'astroTwillight',
      start: calcNormalisedPeriod(0, day),
      end: calcNormalisedPeriod(sunData.nauticalDawn.getTime() - startMs, day),
      colour: '#586a70'
    });
  }

  daylightData.push({
    title: 'nauticalTwillight',
    start: calcNormalisedPeriod(sunData.nauticalDawn.getTime() - startMs, day),
    end: calcNormalisedPeriod(sunData.dawn.getTime() - startMs, day),
    colour: '#6f8f9a'
  });

  daylightData.push({
    title: 'civilTwillight',
    start: calcNormalisedPeriod(sunData.dawn.getTime() - startMs, day),
    end: calcNormalisedPeriod(sunData.sunrise.getTime() - startMs, day),
    colour: '#96bbc9'
  });

  daylightData.push({
    title: 'daylight',
    start: calcNormalisedPeriod(sunData.sunrise.getTime() - startMs, day),
    end: calcNormalisedPeriod(sunData.sunset.getTime() - startMs, day),
    colour: '#b0dae8'
  });

  daylightData.push({
    title: 'civilTwillight',
    start: calcNormalisedPeriod(sunData.sunset.getTime() - startMs, day),
    end: calcNormalisedPeriod(sunData.dusk.getTime() - startMs, day),
    colour: '#96bbc9'
  });

  daylightData.push({
    title: 'nauticalTwillight',
    start: calcNormalisedPeriod(sunData.dusk.getTime() - startMs, day),
    end: calcNormalisedPeriod(sunData.nauticalDusk.getTime() - startMs, day),
    colour: '#6f8f9a'
  });

  if (
    sunData.night.getTime() &&
    isNightStartThisDay(sunData.night.getTime(), startMs)
  ) {
    daylightData.push(
      {
        title: 'astroTwillight',
        start: calcNormalisedPeriod(
          sunData.nauticalDusk.getTime() - startMs,
          day
        ),
        end: calcNormalisedPeriod(sunData.night.getTime() - startMs, day),
        colour: '#586a70'
      },
      {
        start: calcNormalisedPeriod(sunData.night.getTime() - startMs, day),
        end: calcNormalisedPeriod(MILLISECONDS_IN_DAY, day),
        colour: '#2f454d'
      }
    );
  } else {
    daylightData.push({
      title: 'astroTwillight',
      start: calcNormalisedPeriod(
        sunData.nauticalDusk.getTime() - startMs,
        day
      ),
      end: calcNormalisedPeriod(MILLISECONDS_IN_DAY, day),
      colour: '#586a70'
    });
  }

  return daylightData;
}

function isNightStartThisDay(nightStartMs, dayStartMs) {
  return (nightStartMs - dayStartMs) / MILLISECONDS_IN_DAY <= 1;
}

function calcNormalisedPeriod(period, day) {
  const msInWeek = 7 * MILLISECONDS_IN_DAY;
  const dayOffset = day * MILLISECONDS_IN_DAY;
  return (period + dayOffset) / msInWeek;
}
