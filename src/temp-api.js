const URL = 'http://localhost:4400';

export function getHistoricalValues() {
  return fetch(`${URL}/historical`).then(d => d.json());
}

export function getWeekData() {
  return fetch(`${URL}/week-data`).then(d => d.json());
}
